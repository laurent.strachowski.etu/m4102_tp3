package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public class PIzzaDao {

	 @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, ingredients List)")
	  void createTable();

	  @SqlUpdate("DROP TABLE IF EXISTS pizzas")
	  void dropTable();

	  @SqlUpdate("INSERT INTO pizzas (name,ingredients) VALUES (:name, :ingredients)")
	  @GetGeneratedKeys
	  long insert(String name, List<Ingredients> ingredients);

	  @SqlQuery("SELECT * FROM pizzas")
	  @RegisterBeanMapper(Ingredient.class)
	  List<Ingredient> getAll();

	  @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	  @RegisterBeanMapper(Ingredient.class)
	  Ingredient findById(long id);
	  
	  @SqlQuery("SELECT * FROM ingredients WHERE name = :name")
	  @RegisterBeanMapper(Ingredient.class)
	  Ingredient findByName(String name);

	  @SqlUpdate("DELETE FROM ingredients WHERE id = :id")
	  void remove(long id);
}
