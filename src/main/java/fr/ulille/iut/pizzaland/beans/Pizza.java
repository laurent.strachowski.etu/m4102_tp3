package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;

public class Pizza {
	private ArrayList<Ingredient> ingredients;
	private int id;
	private String name;
	
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
